#!/bin/bash

curl -L https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64 -o cloudflared
chmod +x ./cloudflared
printf "Host ssh.srikanthk.tech\nProxyCommand $PWD/cloudflared access ssh --hostname #h" | tr "#" "%" >> ~/.ssh/config
